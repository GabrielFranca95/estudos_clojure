(ns hospital2.aula3
  (:use clojure.pprint)
  (:require [hospital2.logic :as h.logic]))

(defn carrega-paciente [id]
  (println "Carregando" id)
  (Thread/sleep 5000)
  { :id id, :carregado-em (h.logic/agora)})

;(pprint (carrega-paciente 15))
;(pprint (carrega-paciente 30))

; função pura ;)
;pergunta se o id ja exieste em pacientes se ja existe ele não vai carregar
;vai pegar somente o cache direto, se não existe ele vai fazer o assoc e carregar o paciente
(defn- carrega-se-nao-existe
  [cache id carregadora]
  (if (contains? cache id)
    cache
    (let [paciente (carregadora id)]
      (assoc cache id paciente))))

;(pprint (carrega-se-nao-existe {}, 15, carrega-paciente))
;
;(pprint (carrega-se-nao-existe { 15 { :id 15 } }, 15, carrega-paciente))



(defprotocol Carregavel
  (carrega! [this id]))

(defrecord Cache [cache carregadora]
  Carregavel
  (carrega! [this id]
    (swap! cache carrega-se-nao-existe id carregadora)
    (get @cache id)))


(def pacientes (->Cache (atom {}), carrega-paciente))
(pprint pacientes)
(carrega! pacientes 15)
(carrega! pacientes 30)
(carrega! pacientes 15)
(carrega! pacientes 12)
(carrega! pacientes 18)
(carrega! pacientes 1)
(carrega! pacientes 2)
(carrega! pacientes 3)


(pprint pacientes)