(ns treinando.treino3
  (:use clojure.pprint))


;--------------------------- pop ----- peek -------------------------------------

; O pop nos retorna todos os elementos de uma lista exceto o último elemento adicionado
;user=> (peek [1 2 3])
;3
;user=> (pop [1 2 3])
;[1 2]
;user=> (peek '(1 2 3))
;1
;user=> (pop '(1 2 3))
;(2 3)

; Como vemos acima o pop nos trouxe todos os elemtos do vetor menos o
; último, pois o vetor por padrão adiciona o novo elemento ao final
; e nos trouxe todos os elementos de uma lista ligada sem o primeiro
; pois a lista ligada por padrão adiciona o novo elemento no início

; E o peek faz o contrario nos trazendo o ultimo elemente que foi adicionado na lista


;------------------------- keep -------------------------------------------
; O keep nos retorna uma sequencia de valores não nulo de uma lista

;-------------------------- Atomo e swap ----------------------------------

;Para utilizarmos a mutabilidade de forma agradavel, utilizamos o atamo e swap,
;a explicação completa esta disponivel no arquivo aula3 na pasta hospital





;--------------------------- OO ---- Record -------------------------------

; Abstraindo uma ideia de utilizar id para identificar elementos de um mapa
; similar ao uso de objetos em outras linguagens, podemos utilizar
; como no exemplo a seguir; criando uma função chamada adiciona-paciente onde
;temos como parametro pacientes que será um novo mapa listando os dados do paciente
; como nome, id, nascimento etc...
; e um segundo argumento sendo chamado paciente que será o paciente a ser adicionado
; na lista informada no primeiro argumento
; em seguida adicionamos um if-let para informar que se houver uma ecessão
; venhamos informar que o id não foi passado
;
;Em seguida criamos uma função chamada testa-uso-de-pacientes sem argumentor
;e passamos um let com o symbolo pacientes apontando para um mapa vazio, mapa este
; que será preenchido com os dados dos pacientes, lembrando que este "pacientes" é o
; primeiro argumento da função adiciona-pacientes
; cotinuando, apos criar o symbolo pacientes com o mapa vazio, criamos os pacientes em si
; sendo novos symbolos contendo mapas com os dados das pessoas sendo estas pessoas
; guilherme, daniela, paulo
; Porém vamos testar nossa excessão trocando propositalmente o Keyword de paulo
;por um nome qualquer como por exemplo esqueci-de-colocar-id
;por fim damos alguns pprints e chamamos a função para ver o que ocorre...
; e temos um erro pois nossa estrutura depende que seja informado um id


(defn adiciona-paciente [pacientes paciente]
  (if-let [id (:id paciente)]
    (assoc pacientes id paciente)
    (throw (ex-info "paciente not found id" {:paciente paciente}))))

(defn testa-uso-de-pacieentes []
  (let [pacientes {}
        guilherme {:id 15, :nome "guilherme", :nascimento "18/09/1981"}
        daniela {:id 20, :nome "daniela", :nascimento "18/09/1982"}
        paulo {:esqueci-de-colocar-id "paulo", :nascimento "18/10/1983"}]
    (pprint (adiciona-paciente pacientes guilherme))
    (pprint (adiciona-paciente pacientes daniela))
    (pprint (adiciona-paciente pacientes paulo))))

(testa-uso-de-pacieentes)
;{15 {:id 15, :nome "guilherme", :nascimento "18/09/1981"}}
;{20 {:id 20, :nome "daniela", :nascimento "18/09/1982"}}
;Execution error (ExceptionInfo) at treinando.treino3/adiciona-paciente (treino3.clj:4).
;paciente not found id


;Record
;Digamos que não queiramos exigir que o id exista na nossa composição de paciente
;digamos que queiramos que seja opcional o uso de id, para fazer isto podemos
;utilizar o record, com o record nós criamos uma class em java e podemos utilizar
;tanto com o construtor fazendo uso das vantagens de ser rodado direto pelo java
;como utilizar como um mapa e assim tornar opcional até a quantidade de dados a ser exigida como nome, e nascimento, altura etc...
; para exemplificar criaremos uma função record, para tal chamamos como defrecord
(defrecord Paciente [id, nome, nascimento])

;abaixo estamos utilizando o construtor java para criar os três argumentos do record
(pprint (->Paciente 15 "guilherme" "18/09/1981"))

;Já neste segundo exemplo temos uma variação do mesmo construtor
(pprint (Paciente. 15 "guilherme" "18/09/1981"))

; e agora alteramos as posições para mostrar que não há diferença se o argumento é string ou numero
; porém não é possivel informar menos ou mais argumentos do que o especificado no defrecord
(pprint (Paciente. "guilherme" 15 "18/09/1981"))

; Agora neste ultimo exemplo utilizaremos um mapa em clojure, temos uma perca de performance
;porém tudo depende do objetivo da construção da sua aplicação
;o nosso mapa não obriga o uso de um id, sendo possivel seguir mesmo sem um argumento como no
;exemplo abaixo
(pprint (map->Paciente {:nome "guilherme", :nascimento "18/09/1981"}))
;{:id nil, :nome "guilherme", :nascimento "18/09/1981"}

; OU até mesmo adicionar itens a mais que não estavam especificados nos argumentos do defrecord
(pprint (map->Paciente {:id 15 :nome "guilherme", :nascimento "18/09/1981" :altura 1.75}))
;{:id 15, :nome "guilherme", :nascimento "18/09/1981", :altura 1.75}


;---------------------------------- Protocols ------ extend type ----------------------------------------

;Seguindo com o uso de record imagine uma situação em que temos uma clinica hospitalar
; disponibilizando para seus pacientes duas formas diferentes de adesão, a primeira com uso particular
; e a segunda sendo com uso em forma de plano de saude, onde os usuários do plano não precisam assinar nada
;pois ja será cobrado direto o valor dos exames em seu plano, e o paciente particular terá que
; assinar uma autorização dos pagamentos iguais ou acima de 50 para não ocorrer altas cobranças
; desconhecidas para o paciente particular
; e é em cima desta situação de autorização que vamos construir nosso codigo.
; neste caso utilizaremos o record e criaremos
; os dois modelos de pacientes com as requisições dos dados a serem inseridos
; após isso criaremos uma função defn com os argumentos [paciente procedimento valor] após passaremos uma condicional
; informando que se o paciente for igual ao symbolo PacienteParticular iremos para segunda condição que é
; se o valor é igual ou maior que 50.
; abaixo temos o let plano que faz um get nos pacientes do plano estando no plano não é necessário assinar a autorização
; e caso ocorra algo além ele devolverá true como visto na ultima linha, que informará que deve ser assinado a autorização


(defrecord PacienteParticular [id, nome, nascimento])
(defrecord PacientePlanoDeSaude [id, nome, nascimento, plano])


; (defn deve-assinar-pre-autorizacao? [paciente procedimento valor]
  ; (if (= PacienteParticular (type paciente))
  ;(>= valor 50)
  ; (if (= PacientePlanoDeSaude (type paciente))
  ; (let [plano (get paciente :plano)]
  ;(not (some #(= % procedimento) plano)))
  ;true)) )


  ; Uma outra forma de tratarmos isto é com protocols, abaixo criamos um protocolo cobravel
  ; com nome de deve-assinar-pre-autorização? e os paramentros [paciente procedimento valor]
  ; agora considerando o record acima e que é a nossa classe, nós podemos extender esse tipo de classe
  ; similar a herença em orientação a objetos.
  ; criaremos essa extenção com o extend-type que basicamente é uma extenção que usaremos para o nosso
  ; protocolo do record, com os paciente particular e pacientes do plano.
  ; sendo que cada paciente é uma extend-type e cobravel é o protocolo em sí
  ; sendo necessário somente perguntar se o valor está acima de 50 no caso do plano particular
  ;por fim para testar criamos um symbolo com o let e passamos o type do paciente sendo particular
  ; apoós passamos o que será esse simbolo particular ou seja a classe particular informando os dados
  ; esperados no record idade, nome, nascimento
  ; após chamamos o pprint e o protocol informando os dados aguardados paciente sendo particular
  ; procedimento sendo raio-x e valor sendo 25. Por fim ele deve retornar false pois o valor é baixo

;Agora para exemplificar o extend-type do paciente do plano, criamos o paciente passamos os argumentos
; criamos o symbolo plano e passamos os valores e passamos um some verificando se o procedimento esta no plano
; agora adicionamos o plano no nosso último let acima do pprint, e informamos os dados esperados pelo record como
;idade sendo 15, nome sendo ana, nascimento em 18/09/1981 e o outro argumento sendo os exames e procedimentos coberto pelo plano
; sendo que este argumento é um vetor com todos dentro dele, sendo raio-x, ultrassom, e coleta de sangue,
; logo se o exame não estiver dentro deste vetor o resultado será true, para assinar a pré autorização
; pois o plano não cobre esses procedimentos

  (defprotocol Cobravel
    (deve-assinar-pre-autorizacao? [paciente procedimento valor]))


  (extend-type PacienteParticular
    Cobravel
    (deve-assinar-pre-autorizacao? [paciente, procedimento, valor]
      (>= valor 50)))


  (extend-type PacientePlanoDeSaude
    Cobravel
    (deve-assinar-pre-autorizacao? [paciente, procedimento, valor]
      (let [plano (:plano paciente)]
        (not (some #(= % procedimento) plano)))))


  (let [particular (->PacienteParticular 15, "Joáo", "18/9/1981")
        plano (->PacientePlanoDeSaude 15, "ana", "18/9/1981", [:raio-x, :ultrasom, "coleta de sangue"])]
    (pprint (type particular))
    )


