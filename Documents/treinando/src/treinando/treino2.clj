(ns treinando.treino2)


;------------------------------ REDUCE ----------------------------------------
(println "RODANDO FUNÇÃO REDUCE")
;Reduce pega um conjunto de valores e reduz a um único valor
;Chamamos a função reduce e passamos uma segunda função a ser aplicada a lista
;Reduce aplica a função a todos os elementos reduzindo a um só
(reduce + 1 [2 3 4 ])
;=> 10

;caso não seja passado um valor após a função ele o considerará como nulo
(reduce + [2 3 4 ])
;=> 9

;exemplo em função
(defn meu-reduce [arg1 arg2]
  (+ arg1 arg2))
(reduce meu-reduce 1 [2 3 4])
;=> 10
;chamndo meu-reduce e os argumentos arg1(1) e arg2([2 3 4])




;------------------------------- MAP ------------------------------------------
(println "\n\n\n\nRODANDO MAP")
;map aplica uma função em todos os elementos de uma lista retornando uma nova lista
(map inc [2 3 4])
;=> (3 4 5)

;exemplo em função
(defn meu-map [arg]
  (+ 2 arg))
(map meu-map [ 2 3 4 ])
;=> (4 5 6)

(defn meu-map2 [arg]
  (* 2 arg))
(map meu-map2 [ 2 3 4 ])
;=> (4 6 8)

(defn meu-map3 [arg]
  (* 2 arg))
(map meu-map3 [ 2 3 4 ])

; a função meu-map4 tem 2 argumentos sendo passado um let para especificar
;que o symbolo "primeiro" será o primeiro elemento do argumento "lista"
;e declaramos o retorno da função sendo o primeiro argumento e "primeiro"
; ou seja uma lista sendo passada no segundo argumento resulta na exibição
; do primeiro valor da lista
(defn meu-map4 [funcao lista]
  (let [primeiro (first lista)]
    (funcao primeiro)))
;agora chamamos "meu-map4" e chamamos os dois argumentos sendo o primeiro
; argumento a função println e o segundo argumento a lista [2 3 4]
(meu-map4 println [2 3 4])
;2
;=> nil

;-------------------------------- RECURSIVIDADE ------------------------------------------
(println "\n\n\n\n RODANDO RECURSIVIDADE")
; A recursividade parecida com o 'loop' faz com que um laço seja criado
; a função meu-map5 possui 2 argumentos "funcao" e "lista". Logo abaixo criamos um
;symbolo chamado "primeiro" cujo valor é solicitar o primeiro elemento do argumento lista
;a seguir passamos um if, informando que se o symbolo primeiro for verdadeiro
;execute o retorno abaixo, sendo o retorno o argumento "funcao" e o symbolo "primeiro"
;cujo valore é o primeiro elemento do argumento "lista". Porém isso criaria um loop
;teoricamente infinito, uma vez que ao acabar os elementos da lista o laço
;continuaria a retornar a lista vazia. Para evitarmos isto, adicionamos
;uma nova declaração chamando a propria função meu-map5 e depois chamando
;o primeiro argumento "funcao"e depois o restante dos elementos do argumento "lista"
;um de cada vez, fazendo a função se tornar um laço recursivo
(defn meu-map5 [funcao lista]
  (let [primeiro (first lista)]
    (if primeiro
      (do
        (funcao primeiro)
        (meu-map5 funcao (rest lista))))))
;(meu-map5 println [ 34 54 89])
;34
;54
;89
;=> nil

(println "\n\n\n\n RODANDO RECUR")

;----------------------------IF NOT NIL?-----------  TAIL RECUR  -------------------
;Agora iremos fazer uma modificação no if, pois se tivermos uma lista que cotenha um
;valor passado como 'false' o laço não será executado, então faz mais sentido usarmos
; o if para verificar se o valor é "nulo", desta forma usamos o not e "nil?" declarando
;que se o valor não for nulo execute o restante da função

;Ao chamar a propria função temos um limite de chamadas como por exemplo em um range
;para otimizar um laço utilizamos a função 'recur'. Fazendo assim
;consegumos a recursividade sem ter que chamar a propria função
;Para utilizar o recur devemos passalo ao final da função ou seja na cauda, por isso o nome
;conhecido como recursão de cauda, se não o fizer desta forma teremos um erro no log
(defn meu-map6 [funcao lista]
  (let [primeiro (first lista)]
    (if (not (nil? primeiro))
      (do
        (funcao primeiro)
        (recur funcao (rest lista))))))
(meu-map6 println [ 2 3 5])
;2
;3
;5
;=> nil

(println "\n\n\n\n RODANDO MAP")
;------------------------------------ mapa -----------------------------------------

;utilizando mapas com cerquilhas a ideia é que funcionem como se fossem indices
;de um livro, onde podemos observar o indice(keywords) e acessar os capítulos(valores)
;sendo também possíel que cada capítulo se comporte como uma keyword diante dos seus tópicos
;tornando assim que um valor de uma keyword também possa ser uma chave para um novo mapa

;Abaixo temos alguns mapas que tem como nome de symbolo as turmas de uma escola
;e cada mapa representa um aluno como descrito na primeira keyword de cada mapa
;cada aluno tem seu nome e seus itens sendo que o aluno bruno faz parte de duas turmas
;ao fim dos mapas temos uma função que chama os alunos e exibe as turmas em que os
;alunos estão matriculados

(def turma-portugues {:aluno 1
                      :nome "bruno"
              :itens   {:mochila  {:id :mochila, :quantidade 2}
                        :camiseta {:id :camiseta, :quantidade 3}
                        :tenis    {:id :tenis, :quantidade 1}
                        :calca    {:id :calca, :quantidade 2}
                        :livros   {:id :livros, :quantidade 7}}})

(def turma-historia {:aluno 5
                     :nome "ana"
             :itens   {:mochila  {:id :mochila, :quantidade 2}
                       :camiseta {:id :camiseta, :quantidade 3}
                       :tenis    {:id :tenis, :quantidade 1}
                       :calca    {:id :calca, :quantidade 2}
                       :livros   {:id :livros, :quantidade 7}}})

(def turma-matematica {:aluno 1
                       :nome "bruno"
             :itens   {:mochila  {:id :mochila, :quantidade 2}
                       :camiseta {:id :camiseta, :quantidade 3}
                       :tenis    {:id :tenis, :quantidade 1}
                       :calca    {:id :calca, :quantidade 2}
                       :livros   {:id :livros, :quantidade 7}}})

(defn chama-alunos []
  [turma-portugues, turma-historia, turma-matematica])
(println (chama-alunos))

(println (map count (vals (group-by :aluno (chama-alunos)))))


;Digamos que gostariamos de ter informações um pouco mais claras
;Então podemos utilizar um mapa para obter informações
;Já temos uma função acima(chama-alunos) que busca a informação dos symbolos(materias portugues,matematica...)
;que nos retorna o número de materias que cada aluno está Matriculado,
;mas agora vamos ir além e iremos criar uma função chamada
;"turmas-matriculado" e passamos os argumentos alunos e matérias
;Para obter as informações dos alunos criamos um mapa e passamos uma chave
;chamada aluno-id e passamos o valor "aluno" uma vez que esta função está sendo mapeada
;pela função chama-alunos, os dados da turma são buscados, já os dados dos alunos
;serão trazidos em forma agrupada pelo group-by
;utilizando um println teremos o id aluno e o total de materias em que ele está matriculado



;por q na verdade o que o mapa esta contando é o numero de mapas que cada aluno tem
(defn turmas-matriculado [[aluno quantidade-materias]]
  {:aluno-id aluno
   :total-de-materias (count quantidade-materias)})

(->> (chama-alunos)
     (group-by :aluno)
     (map turmas-matriculado)
     println)

;   Toda a informação é extraida do group-by os valores (2 1), sendo que agora estamos organizando através
;da thread last, informando que vamos pegar aluno, só que passando ele como um value e assim pegar o id dele
;podendo ser o id do "aluno 1" ou "aluno 5".

;   E o segundo argumento "quantidade-materias" como o número de vezes que o "aluno" aparece, essa informação já aparecia ao chamarmos "(println (map count (vals (group-by :aluno (chama-alunos)))))"
;tendo a dedução e informado que é o total de matérias em que o aluno está é o segundo argumento, lembrando que este
;segundo argumento já estava sendo solicitado antes somente com o group-by com o "(println (map count (vals (group-by :aluno (chama-alunos)))))"
;mas agora estamos nomeande ele dentro do mapa, e o nome que estamos passando é "total-de-materias".

;   Antes também estavamos usando o count dentro do println,
; agora com a treed last não estamos usando mais. Por isso precisamos
;passar o count como valor na chave de mapa "total-de-materias".
; Feito isso e solicitando pelo group-by o valor de aluno e passado como um mapa e a quantidade de vezes
;que ele aparece é informada como o número de turmas que ele está matriculado




;------------------------------------ modificando mapa --------------------------------

;Para modificar um mapa podemos utilizar assoc-in, criamos os mapas abaixo
(def users [{:name "James" :age 26} {:name "John" :age 43}])
; E utilizamos o assoc-in para modificalos, passando o "0"como a posição do primeiro mapa a chave "password" como
;nova chave a ser adicionada no mapa e o value "nhoJ"
(assoc-in users [0 :password] "nhoJ")
;=> [{:name "James", :age 26, :password "nhoJ"} {:name "John", :age 43}]

;Podemos visualizar a modificação com o get-in
(println (get-in users [0]))
;{:name James, :age 26}

;Ou se quisermos um valor específico simplesmente informamos a chave
(println (get-in users [0 :name]))
;James

; Também podemos utilizar o update-in, porém com esta função nós podemos além de modificar por outro valor
;como o proprio nome prezume, também podemos incrementa-lo com a ajuda do inc
(update-in users [0 :age] inc)
;=> [{:name "James", :age 27} {:name "John", :age 43}]

;como podemos ver a idade de james foi aumentada em 1 ano por conta do inc



;------------------------------- ORDENAÇÃO -----------------------------------

;Podemos criar ordenação de diversas formas diferentes algumas delas são "sort-by" que nos retorna
;ua lista ordenada mediante a função que informarmos apos chama-lo
(sort-by count ["graaanndeeeee" "meeediiioooo" "pequeno"])
;=> ("pequeno" "meeediiioooo" "graaanndeeeee")

;note que no exemplo acima utilizamos o count como parametro para especificar uma contagem quantitativa como
;padrão para o resultado informando a ordem.

(sort-by :number [{:number 9} {:number 8} {:number 7} {:number 6}])
;=> ({:number 6} {:number 7} {:number 8} {:number 9})

;ao utilizar sort-by e a chave namber temos a ordenação dos valores das chaves
; E para ordenalos de forma reversa utilizamos o reverse
(reverse (sort-by :number [{:number 9} {:number 8} {:number 7} {:number 6}]))
;=> ({:number 9} {:number 8} {:number 7} {:number 6})




;-------------------------------- some -----------------------------------------

; O some verifica se o valor existe na lista e nos retorna true ou nil

(some true? [false false false])
;=> nil

(some true? [false true false])
;=> true

