(ns hospital.aula3
  (:use [clojure pprint])
  (:require [hospital.logic :as h.logic]
            [hospital.model :as h.model]))

; Abaixo temos um exeplo de uso de atmos e swap, utilizando atomos para possibilitar a mutabilidade
; apesar de termos o assoc e conj os mudanças se não forem capturadas se perdem, por isso
; para termos a persistência dos dados salvos utilizamos o swap!
; abaixo temos um exemplo utizando uma função defn chamada "testa-atomao"
; onde temos um let cujo symbolo é hospital-silveira
; que contém um atomo que torna o mapa a seguir mutavel, o mapa tem a chave ":espera"
; e o valor é "fila-vazia" requisitada do name space hospital.model modificado pelo "as"
; para h.model, a seguir damos um print em hospital-silveira para exibir a fila
; logo após um pprint dereferenciando o hospital-silveira e abaixo vazemos uma variação
; do deref usando @ que trará o mesmo resultado

(defn testa-atomao []
  (let [hospital-silveira (atom { :espera h.model/fila_vazia})]
    (println hospital-silveira)
    (pprint hospital-silveira)
    (pprint (deref hospital-silveira))
    (pprint @hospital-silveira)

    ; essa é (uma das) a maneira de alterar conteudo dentro deum atomo
    ; Seguindo na mutabilidade, podemos utilizar o swap! para alterar um valor
    ; como mostra o exemplo abaixo, passamos a função swap, depois o hospital-silveira
    ; em seguida o assoc depois a chave do mapa e por último o valor
    (swap! hospital-silveira assoc :laboratorio1 h.model/fila_vazia)
    (pprint @hospital-silveira)

    (swap! hospital-silveira assoc :laboratorio2 h.model/fila_vazia)
    (pprint @hospital-silveira)

    ; update tradicional imutavel, com dereferencia, que nao trara efeito
    ; neste exemplo o valor será atualizado porem não permanecerá alterado
    (update @hospital-silveira :laboratorio1 conj "111")

    ; indo pra swap mutavel o valor fica permanentemente alterado
    (swap! hospital-silveira update :laboratorio1 conj "111")
    (pprint hospital-silveira)


    ))

(testa-atomao)










