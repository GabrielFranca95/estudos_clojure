(ns treinando.treino4
  (:use clojure.pprint))


;--------------------------------------- Defmult -- DEfmethod ---------------------------------------------------

(defn tipo-de-exame [exame]
  (let [paciente (:paciente exame)
        situacao (:situacao paciente)]
    ;se for urgente sempre vai ser autorizado
    (cond (= :urgente situacao) :sempre-autorizado
          ;se paciente contem a keyword plano logo ele vai ter plano de saude
          (contains? paciente :plano) :plano-de-saude
          :else :particular)))

(defmulti deve-assinar-pre-autorizacao? tipo-de-exame)

(defmethod deve-assinar-pre-autorizacao? :sempre-autorizado [exame]
  false)

(defmethod deve-assinar-pre-autorizacao? :plano-de-saude [exame]
  ;se o exame não for igual a algum dos procedimentos que estiver no plano de saude como
  ;coleta de sangue, a função retorna true
  (not (some #(= % (:procedimento exame)) (:plano (:paciente exame)))))

(defmethod deve-assinar-pre-autorizacao? :particular [exame]
  ;se a primeira posição do valor de exame for igual ou maior que 50 retorne true
  (>= (:valor exame 0) 50))




(let [particular {:id 15, :nome "Guilherme", :nascimento "18/9/1981", :situacao :urgente}
      plano {:id 15, :nome "Guilherme", :nascimento "18/9/1981", :situacao :urgente, :plano [:raio-x, :ultrasom, :coleta-de-sangue]}]

  (pprint (deve-assinar-pre-autorizacao? {:paciente particular, :valor 1000, :procedimento :coleta-de-sangue}))
  (pprint (deve-assinar-pre-autorizacao? {:paciente plano, :valor 1000, :procedimento :cirurgia-plastica})))




(let [particular {:id 15, :nome "Guilherme", :nascimento "18/9/1981", :situacao :urgente}
      plano {:id 15, :nome "Guilherme", :nascimento "18/9/1981", :situacao :normal, :plano [:raio-x, :ultrasom]}]

  (pprint (deve-assinar-pre-autorizacao? {:paciente particular, :valor 1000, :procedimento :coleta-de-sangue}))
  (pprint (deve-assinar-pre-autorizacao? {:paciente plano, :valor 1000, :procedimento :coleta-de-sangue})))


